# Changelog for `informeren-dagens-epub`


## 1.0.0 (2018-05-23)

First public release, featuring:

 * EPUB extraction of any issue available from [the archive at information.dk](https://www.information.dk/dagensavis)
   (defaults to most recent issue).
 * Support for cookie extraction from Firefox or a JSON cookie file.
 * Support for printing relevant cookies in order to create a JSON cookie file.
